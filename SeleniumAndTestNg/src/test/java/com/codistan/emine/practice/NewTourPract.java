package com.codistan.emine.practice;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class NewTourPract {
	
	
		
		public WebDriver driver;
		@Test(groups= {"smokeTest","regression"})
		@Parameters({"url","APIKey/username"})

	public void testNg(String url, String key) {
			
			
			
			 WebDriverManager.chromedriver().setup();
			 driver = new ChromeDriver();
			 
			 driver.get("http://newtours.demoaut.com/");
			 java.lang.String PageSource ;
			PageSource= driver.getPageSource();
	
		
	//WebElement userName = driver.findElement(By.name("userName"));
	//WebElement password = driver.findElement(By.name("password"));
	//WebElement signinButton = driver.findElement(By.name("login"));
	//userName.sendKeys("tutorial");
	//password.sendKeys("tutorial");
	//signinButton.click();
			 

	}
		
		
	@FindBy(xpath ="//input[@name='userName']")
	public WebElement userName;

	@FindBy(xpath ="//input[@name='password']")
	public WebElement passWord;

	@FindBy(xpath ="//input[@name='login']")
	public WebElement clickButton;
	public void logIn(String userN, String passW) {
		
		userName.sendKeys("tutorial");
		passWord.sendKeys("tutorial");
		clickButton.click();
	}
		



@Test(description="assertEquals")
		public void assertEquals() {
		 	
		 	String actualTitle = driver.getTitle();
		 	System.out.println(actualTitle);
		 	
		 	String expectedTitle = "NewTour";
		 	Assert.assertEquals(actualTitle, expectedTitle);
		 	

	}
@DataProvider
public void  getData() {
	Object[][] data=new Object[2][2];
	data[0][0]= "username";
	data[0][1]="password";
}
	

}

