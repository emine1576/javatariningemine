package com.codistan.Emine.lesson09;

import java.util.Iterator;

public class NestedLoops {

	public static void main(String[] args) {
		for (int i = 0; i <3; i++) {
			System.out.println("outer loop, cycle:" + i);
			for (int j = 0; j <3; j++) {
				System.out.println("inner loop,cycle: " + j);
				
			}
			
		}
		for (int i = 0; i <10; i++) {
			for (int j = 0; j <=10; j++) {
				System.out.println(i +" x " + j + " = " + (i*j));
				
			}
			System.out.println("");
		}
		
		
		for (int i = 0; i <10; i++) {
			for (int j = 0; j <=10; j++) {
				System.out.println(i+"+" +j + "=" + (i+j));
				
			}
			
		}
		
	}

}
