package com.codistan.Emine.lesson09;

public class DoWhileExercise {

	public static void main(String[] args) {
		int counter = 0;
		while (counter < 100) {
			System.out.println("Current number with while " + counter);
			counter++;
		}

		// Please print numbers from 0 to 100 by using do-while loop
		counter = 0; // resetting the counter
		do {
			System.out.println("Current number with do-while " + counter);
			counter++;
		} while (counter < 100);

		// Please print numbers from 110 to 100 by using while loop
		counter = 110;
		while (counter >= 100) {
			System.out.println("Current number with while " + counter);
			counter--;
		}

		// Please print numbers from 110 to 100 by using while loop
		counter = 110; // resetting the counter
		do {
			System.out.println("Current number with do-while " + counter);
			counter--;
		} while (counter >= 100);

		// Please print even numbers from 100 to 200 such as 100, 102, 104, ...
		counter = 100;
		while (counter <= 200) {
			System.out.println("Current number with while " + counter);
			counter = counter + 2;
		}

		counter = 100;
		while (counter <= 200) {
			if (counter % 2 == 0) {
				System.out.println("Current number with while " + counter);
			}
			counter++;
		}
		
		//
		int dayOfTheWeek = 7; 
		switch (dayOfTheWeek) {
		case 1:
			System.out.println("It is monday, feeling very sad.");
			break;//get out of the block.mean cury braces
		case 2:
			System.out.println("It is tuesday, feeling less sad.");
			break;
		case 3:
			System.out.println("It is wednesday, feeling not sad.");
			break;
		case 4:
			System.out.println("It is thursday, feeling happy.");
			break;
		case 5:
		// print even  numbers  with for loop

			System.out.println("It is friday, feeling more happy.");
			break;
		case 6:
			System.out.println("It is saturday, feeling very happy.");
			break;
		case 7:
			System.out.println("It is sunday, feeling crazy happy.");
			break;
		default:
			System.out.println("Wrong day selection.");
			break;
		}
		
	String myStringArray[] = {"Hello","world","and","galaxy"};
	for (int i = 0; i < myStringArray.length; i++) {
		System.out.println(myStringArray[i].toUpperCase());
		
		}	
		for (int i =myStringArray.length-1;i>=0;i--) {
		System.out.println(myStringArray[i].toUpperCase());	
		}
		
	}
}
			
			
				
				
			
	

