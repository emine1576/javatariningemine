package com.codistan.Emine.lesson09;

public class MoreLoopsTwo {

	public static void main(String[] args) {
		/*
		 * Please create a String array by using the following sentence 
		 * and print each word one by one to the screen. 
		 * Ex: Selenium is an umbrella project for a range of tools and 
		 * libraries that enable and support the automation of web browsers.
		 */
		String strparagr = "Selenium is an umbrella project for a range of tools and libraries that enable and support automation of web browsers.";
		String[] strArray = strparagr.split(" ");
		for (int i = 0; i < strArray.length; i++) {
			System.out.println(strArray[i]);
			
			
		}
		System.out.println(strArray.length);
		for (String currentCycleString : strArray) {
			System.out.println(currentCycleString);
	}

}
	/*
	 * It provides extensions to emulate user interaction with browsers, a distribution server for scaling browser allocation, and the infrastructure for implementations of the W3C WebDriver specification that lets you write interchangeable code for all major web browsers.

By using the paragraph above, please print each word one by one by using enhanced loop.
	 */
	String secondparagr =" It provides extensions to emulate user interaction with browsers, a distribution server for scaling browser allocation, and the infrastructure for implementations of the W3C WebDriver specification that lets you write interchangeable code for all major web browsers.  ";
	
}

