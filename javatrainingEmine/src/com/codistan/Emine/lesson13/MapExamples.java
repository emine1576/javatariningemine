package com.codistan.Emine.lesson13;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapExamples {

	public static void main(String[] args) {
		HashMap<String,String> myHM= new HashMap<>();
		myHM.put("C1-2-23", "harry porter");
		myHM.put("F2-5-10", "Star wars");
		myHM.put("T5-1-21", "Java 8 fundementals");
		myHM.put("F2-5-10","The nightangale");
		myHM.put("S2-4-24", "Biology");
		myHM.put("F2-5-21", "the Alcehemist");
		System.out.println(myHM);
		System.out.println(myHM.get("F2-5-10"));
		System.out.println(myHM.get("S2-4-24"));
		System.out.println(myHM.get("F2-5-21"));
		System.out.println(myHM.values());
		System.out.println(myHM.containsValue("star wars"));
		Set sn = myHM.entrySet();
		Iterator it = sn.iterator();
		while(it.hasNext()) {
		Map.Entry mp =(Map.Entry)it.next();
		System.out.println(mp.getKey());
		System.out.println(mp.getValue());
		
		

	}

}
}
