package com.codistan.java.hasan;

import java.util.Scanner;

public class FlowControl3 {

	public static void main(String[] args) {
		//ternary operator
		
		int num1 = 1;
		int num2 = 3;
		int max = 0;
		max = (num1 > num2) ? num1 : num2;
		System.out.println("Maximum number is: " + max);
//switch statement
		String grade = "F";
		switch(grade) {
		case "A":
			System.out.println("Perfect");
			break;
		case "B":
			System.out.println("Very good");
			break;
		case "C":
			System.out.println("You did Ok");
			break;
		default:
			System.out.println("Study harder");
			break;
			
			
		
	}
		Scanner keyboard = new Scanner(System.in);
		String category = keyboard.nextLine();
		switch(category) {
		case "A":
			System.out.println("You sit in the fron for $80");
			break;
		case "B":
			System.out.println("You sit in the back for $60");
			break;
		case "C":
			System.out.println("You sit in the balcony for $50");
			break;
		default:
			System.out.println("buy ticket");
			break;
		}
	
	
	}
}
		
		


