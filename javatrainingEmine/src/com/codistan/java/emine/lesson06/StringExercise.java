package com.codistan.java.emine.lesson06;

public class StringExercise {

	public static void main(String[] args) {
		//Create a variable that is holding your full name and then print only your last name. 
		String fullName = "Emine yavuz"; 
		String[] words = fullName.split(" ");
		System.out.println(words[1]);
		
		//Print the first and last character of your full name (subString, charAt, length)
		System.out.println(fullName.charAt(0) + "" + fullName.charAt(9));
		System.out.println(fullName.charAt(0) + "" + fullName.charAt(fullName.length()-1)); //more reliable
		System.out.println(fullName.substring(0,1)+ fullName.substring(9));
		
		//Print your full name as last_name, firstname: Adams, John (+ or concat)
		System.out.println(words[1]+ ", " + words[0]);
		System.out.println(words[1].concat(", ").concat(words[0]));
		
		//Insert a nickname between your firstname and lastname and print it. (+ or concat)
		String nickName = "Filiz";
		System.out.println(words[0] + nickName + words[1]);
		
		//print your full name without vowels. A, E, I, O, U, a, e, i, o, u 
		System.out.println(fullName.replaceAll("A", "").replaceAll("a", "").replaceAll("E", "").replaceAll("e", "").replaceAll("I", "").replaceAll("i", "").replaceAll("O", "").replaceAll("o", "").replaceAll("U", "").replaceAll("u", ""));
		
		System.out.println(fullName.replaceAll("[AaEeIiOoUu]", ""));
		
		
		

	}

}

	