package com.codistan.java.emine.lesson06;

public class JavaStringTwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	String firstStr = "John";
	String secondStr = "John";
	String thirdStr = new String("John");
	String forthStr = new String("John");
	String fifthStr = new String("john");
	String emptyStr = new String ();//empty string
	String emptyStr2 = "";//empty string
	String anotherString ;//null
    
	
	//string builders are not threadsafers but more effiecent than string buffets.
	// that is the main diffrence between them
	
	System.out.println(firstStr == secondStr);
	System.out.println(secondStr == thirdStr);
	System.out.println(thirdStr == forthStr);
	System.out.println(forthStr == fifthStr);
	System.out.println();
	System.out.println(firstStr.equals(secondStr)); //when you are comparing String values, always use equals(). 
	System.out.println(secondStr.equals(thirdStr));
	System.out.println(thirdStr.equals(forthStr));
	System.out.println(forthStr.equals(fifthStr));
	
	String seventhStr = "Hello World";
	System.out.println(seventhStr.replaceAll("1", "t"));
	String[]strArray = seventhStr.split(" ");
	System.out.println(strArray[0]);
	System.out.println(strArray[1]);
	System.out.println(seventhStr.endsWith("t"));
	System.out.println(seventhStr.startsWith("H"));
	System.out.println(seventhStr.substring(4,7));
	System.out.println(seventhStr.toLowerCase());
	System.out.println(seventhStr.toUpperCase( ));
	String eightStr = "John";
	System.out.println(eightStr);
	System.out.println(eightStr.trim());
	// we are splitting name
	String fullName = "Emine Yavuz";
	String[] strArray02 = fullName.split(" ");
	// we are joining the strings
	String fullNameTwo = String.join(" ", strArray02);
	System.out.println(fullNameTwo);
	
	String nickName = "filiz";
	
	
	
	
	
	
	
	
	
	}
	
}

