package com.codistan.java.emine.lesson06;

public class StringBuffersAndStringBuilders {

	public static void main(String[] args) {
		StringBuilder myStringBuilder = new StringBuilder ("hello World!");
		System.out.println(myStringBuilder);
		System.out.println(myStringBuilder.append("and Class!"));
		System.out.println(myStringBuilder);
		System.out.println(myStringBuilder.insert(6," Galaxy "));
		System.out.println(myStringBuilder.replace(0, 1,"h"));
		System.out.println(myStringBuilder.delete(13, 19));
		System.out.println(myStringBuilder.reverse());//it is important . might be interview questions
		//how do you reverse string(interview questions)
		String result = myStringBuilder.toString();
		System.out.println(result);
		// i  can create a stirng builder and assign		
		
String input = "My example input";//interview question
StringBuilder tempSB = new StringBuilder(input);
tempSB.reverse();
input = tempSB.toString();
}

}
