package com.codistan.java.emine.lesson12;

public class ThrowExample {
	static int evenNumber=10;

	public static void main(String[] args) throws Exception {
		if(evenNumber%2!=0) {
			throw new Exception("we only allow even numbers");
		}
	System.out.println("my even number"+evenNumber);
	throw new Exception();//this will always throw an exception
	}

}
