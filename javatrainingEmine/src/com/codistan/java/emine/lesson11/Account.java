package com.codistan.java.emine.lesson11;

public abstract class Account {
	public Account(String accountNo, String routingNo, String ownersFullName, double balance) {
		super();
		this.accountNo = accountNo;
		this.routingNo = routingNo;
		this.ownersFullName = ownersFullName;
		this.balance = balance;
	}
	
	String accountNo; // 00002345
	String routingNo; 
	String ownersFullName;
	
	private double balance; 
	abstract protected void depositCash(double amount);
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	

}


