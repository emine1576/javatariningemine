package com.codistan.java.emine.lesson11;


public class BankApplication {

	public static void main(String[] args) {
		CheckingAccount myCA = new CheckingAccount("00001", "12345", "John Doe", 25,45); 
		System.out.println("balance:"+myCA.getBalance());
		System.out.println("account number:"+myCA.accountNo);
		System.out.println("owners full name:"+myCA.ownersFullName);
		System.out.println("routing number:"+myCA.routingNo);
//		myCA.balance = 35000;
		myCA.setBalance(3500);
		System.out.println("balance after setting balance:"+ myCA.getBalance());
		myCA.depositCash(3500);
		System.out.println("balance after depositing cash:" + myCA.getBalance());
		myCA.withdrawMoney(1000);
		System.out.println("balance after withdrawing 1000:" + myCA.getBalance());
		myCA.withdrawMoney(100000);
		System.out.println("balance after withdrawing 100000:" + myCA.getBalance());
		myCA.withdrawMoney(3000);
		System.out.println("balance after withdrawing 3000:" + myCA.getBalance());
		myCA.withdrawMoney(550);
		System.out.println("balance after withdrawing 550:" + myCA.getBalance());
		myCA.withdrawMoney(525);
		System.out.println("balance after withdrawing 525:" + myCA.getBalance());
		
	}
}
