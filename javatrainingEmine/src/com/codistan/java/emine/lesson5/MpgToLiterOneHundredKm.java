package com.codistan.java.emine.lesson5;

public class MpgToLiterOneHundredKm {

	public static void main(String[] args) {
		// given miles driven  and burned gas in galons, calculate liter per 100 kilometers.
		//ex: if your car mpg is 22 then liter per 100 km should be 10.6916
		//235.215/(22 US mpg) = 10.692 L/100km
		double mpg = 22;
	    
	    double results = (235.215)/mpg;
	  
		System.out.println("this car is burning  " + results + "liters/100km");
	    

		
	}

}
