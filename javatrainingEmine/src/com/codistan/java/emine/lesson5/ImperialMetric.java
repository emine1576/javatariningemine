package com.codistan.java.emine.lesson5;

public class ImperialMetric {

	public static void main(String[] args) {
		
				//We will give 5 pound value and program will print the kg value. 
				double poundValue = 5; 
				final double poundToKgConstant = 0.45359237; //final makes a variable constant which means once it is declared, it can not be changed. 
				double resultKg = poundValue * poundToKgConstant;
				System.out.println(poundValue + " pounds = " + resultKg + " kgs.");
				
				//We will give 5 inch value and program will print the cm value. 
				double inchValue = 5;
				final double inchToCmConstant = 2.54;
				double resultCm = inchValue*inchToCmConstant;
				System.out.println(inchValue + "inches =" + resultCm + " cms.");
				
				//We will give 5 mile value and program will calculate the km value. 
				double mileValue = 5;
				final double mileToKmConstant = 1.60;
				double resultKm = mileValue*mileToKmConstant;
				System.out.println(mileValue + "miles =" + resultKm + "kms.");
				
				
				
				//We will give 5 oz value and program will calculate the ml value.
				double ozValue = 5;
				final double ozToMlConstant = 29.5;
				double resultMl = ozValue*ozToMlConstant;
				System.out.println(ozValue + "ozs=" + resultMl + "mls");
				
				
				
				//We will give 5 F value and program will calculate the C value.
				double fValue= 5;
				double resultC = (fValue-32)*(0.5556);
				System.out.println(fValue + "Fs =" + resultC + "Cs");
				
				
				
	

			}

		


	}

