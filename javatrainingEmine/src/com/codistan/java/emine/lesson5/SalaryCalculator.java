package com.codistan.java.emine.lesson5;

public class SalaryCalculator {

	public static void main(String[] args) {
		//  Create a class called SalaryCalculator which will calculate weekly total salary. 
		//An employee can get $13.25 per hour but if he/she works more than 40 hours than 
		//the wage will be calculated by x1.5 so more than regular wage. Application will 
		//calculate total amount based on the hours an employee worked and print it to the console.

		double payRate= 13.25;
		double totalWeeklyHour = 45;
		double overtime = (totalWeeklyHour-40)*(1.5*payRate);
		double regularWage = 40*payRate;
		double totalWage = regularWage + overtime;
		System.out.println("The total wage of employee is " + totalWage);
		
		


     }	
		
	}

