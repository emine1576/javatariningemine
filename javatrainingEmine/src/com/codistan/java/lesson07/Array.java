package com.codistan.java.lesson07;

public class Array {

	public static void main(String[] args) {
		int myIntegers[] = new int[4];
		myIntegers[0] = 10;
		myIntegers[1] = 20;
		System.out.println(myIntegers[0]);
		System.out.println(myIntegers[1]);
		System.out.println(myIntegers[3]);
		
int myIntegersTwo[] = {10,20,30,40};
System.out.println(myIntegersTwo[0]);
System.out.println(myIntegersTwo[1]);
System.out.println(myIntegersTwo[3]);
//Exercise: Create two arrays; one for grocery item names, one for grocery item 
//prices as following: lemon, lettuce, watermelon, orange, spinach and 
//5.99, 2.99, 1.89, 8.00, 2.89 and print each item�s 
//name and price on the same line. 

	}
	

}
