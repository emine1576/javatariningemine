package com.codistan.java.lesson07;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListExample {

	public static void main(String[] args) {
		//array list is biiger advantage, they do samething but arraylist has more 
		//advantages
	ArrayList<String> myArrayList = new ArrayList<>();//how we create array list
	myArrayList.add("hello");
	myArrayList.add("world");
	myArrayList.add("!");
	System.out.println(myArrayList);
	
	myArrayList.remove("!");
	System.out.println(myArrayList);
	myArrayList.remove(0);
	System.out.println(myArrayList);
	System.out.println(myArrayList.size());
	System.out.println(myArrayList.get(0));
	System.out.println(myArrayList.contains("hello"));
	System.out.println(myArrayList.contains("world"));
	System.out.println(myArrayList.indexOf("world"));
	//myArrayList.clear();
	System.out.println(myArrayList);
	Object[]myArray = myArrayList.toArray();
	System.out.println(Arrays.deepToString(myArray));	
	//What is the difference between Array and Array list?
	//it is typical interview question                                                                                            nterview question 
	//important******
	//
	ArrayList<Integer>myIntList = new ArrayList<>();
	myIntList.add(23);
	

	}

}
