package com.codistan.java.lesson07;

public class ArraysExercise1 {

	public static void main(String[] args) {
		// //Exercise: Create two arrays; one for grocery item names, one for grocery item 
		//prices as following: lemon, lettuce, watermelon, orange, spinach and 
		//5.99, 2.99, 1.89, 8.00, 2.89 and print each item�s 
		//name and price on the same line. 
		String[]itemNames = {"lemon","lettuce","watermelon","orange","spinach"};
		double[]itemPrices = {5.99,2.99,1.89,8.00,2.89};
		System.out.println(itemNames [0] + " is " + itemPrices[0]);
		System.out.println(itemNames [1] + " is " + itemPrices[1]);
		System.out.println(itemNames [2] + " " + itemPrices[2]);
		System.out.println(itemNames [3] + " " + itemPrices[3]);
		System.out.println(itemNames [4] + " " + itemPrices[4]);
	
		

	}

}
