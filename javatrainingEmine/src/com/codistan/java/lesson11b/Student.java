package com.codistan.java.lesson11b;

public class Student extends Youth implements BasketballPlayer, SoccerPlayer {
	public Student(String mood) {
		super(mood);
		// TODO Auto-generated constructor stub
	}

	public void study() {
		System.out.println("Student is studying");
	}

	@Override
	public void kickBall() {
		System.out.println("Student is kicking the ball");
		
	}

	@Override
	public void jumpHigh() {
		System.out.println("Student is jumphing high");
		
	}
}