package com.codistan.java.lesson11b;


public class Human {
	
	
	public Human(String mood) {
		super();
		this.mood = mood;
	}
	private String mood=null;//this is a field , it is a class level
	
	public String getMood() {//method
		return mood;
	}
	public void setMood(String mood) {//method
		this.mood=mood;
	}
	public void eat() {
		String temp =null;//this is not a field , local variable
		String temp2=new String();
		String temp3=new String("hello");//you can overload consractors
		
		System.out.println("Human is eating");
	}
	public void eat(String food) {
		System.out.println("human is eating"+food);

	
		
	}
}


